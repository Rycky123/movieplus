package com.example.movieplus.view

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import com.example.movieplus.R
import com.example.movieplus.databinding.ActivityRegisterBinding
import com.example.movieplus.model.Loading
import com.example.movieplus.model.data.database.Dao.Dao
import com.example.movieplus.model.data.database.DatabaseSQL
import com.example.movieplus.model.data.database.Entity.PersonEntity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import java.util.jar.Manifest
import javax.inject.Inject

@AndroidEntryPoint
class RegisterActivity: AppCompatActivity() {

    companion object val PICKIMAGE = 100
    private var imageUri: Uri? = null
    private var foto: String = ""
    private val db = FirebaseFirestore.getInstance()

    lateinit var binding: ActivityRegisterBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

            binding.idFotoUser.setOnClickListener {
                getImage()
            }



        binding.btnComenzarApp.setOnClickListener {

            if (binding.editRegisterNombre.text.toString().isEmpty() || binding.editRegisterTel.text.toString().isEmpty()
                || binding.editRegisterPass.text.toString().isEmpty() || binding.editRegisterCorreo.text.toString().isEmpty() ||
                binding.editRegisterTel.text.toString().isEmpty()){
                if (binding.editRegisterNombre.text.toString().isEmpty()){
                    binding.editRegisterNombre.setError("Campo requerido")
                }
                if (binding.editRegisterTel.text.toString().isEmpty()){
                    binding.editRegisterTel.setError("Campo requerido")
                }
                if (binding.editRegisterPass.text.toString().isEmpty()){
                    binding.editRegisterPass.setError("Campo requerido")
                }
                if (binding.editRegisterCorreo.text.toString().isEmpty()){
                    binding.editRegisterCorreo.setError("Campo requerido")
                }
            }else {

                val nom = binding.editRegisterNombre.text.toString()
                val passt = binding.editRegisterPass.text.toString()
                val tel = binding.editRegisterTel.text.toString()
                val mail = binding.editRegisterCorreo.text.toString()
                if (passt.length > 6){
                    if (tel.length==10){
                       if(!isValidEmail(mail)){
                           Log.e("correo","no valido")
                       }else{
                           Log.e("correo"," valido")
                       }
                        FirebaseAuth.getInstance().createUserWithEmailAndPassword(mail,
                            passt).addOnCompleteListener {
                            if (it.isSuccessful) {
                                ShowHome(this)
                                if(imageUri != null){
                                    val filename = UUID.randomUUID().toString()
                                    val storage = FirebaseStorage.getInstance().getReference("/images/$filename")
                                    storage.putFile(imageUri!!).addOnSuccessListener {
                                    }.addOnSuccessListener {
                                        storage.downloadUrl.addOnSuccessListener {
                                            Toast.makeText(this, it.toString(), Toast.LENGTH_SHORT)
                                                .show()
                                            Log.e("TAG", it.toString())
                                            foto = it.toString()
                                            val user = hashMapOf(
                                                "Foto de perfil" to foto,
                                                "Nombre" to nom,
                                                "Telefono" to tel,
                                                "Correo" to mail,
                                                "Lat" to "0",
                                                "Log" to "0"
                                            )


                                            db.collection("Usuarios").document(mail).set(user)

                                            val databaseHandler: DatabaseSQL = DatabaseSQL(this)
                                            val estatus = databaseHandler.addUser(PersonEntity(0, nom, binding.editRegisterPass.text.toString(),
                                            tel, mail," ", foto))
                                            if(estatus > 0 ){
                                                Toast.makeText(applicationContext, "Agregado con exito ", Toast.LENGTH_LONG).show()
                                            }else {
                                                Toast.makeText(applicationContext, "Algo fallo ", Toast.LENGTH_LONG).show()
                                            }

                                        }
                                    }
                                }else{
                                    val user = hashMapOf(
                                        "Nombre" to nom,
                                        "Telefono" to tel,
                                        "Correo" to mail,
                                        "Lat" to "0",
                                        "Log" to "0")
                                    db.collection("Usuarios").document(mail).set(user)

                                    val databaseHandler: DatabaseSQL = DatabaseSQL(this)
                                    val estatus = databaseHandler.addUser(PersonEntity(0, nom, binding.editRegisterPass.text.toString(),
                                        tel, mail," ", " "))
                                    if(estatus > 0 ){
                                        Toast.makeText(applicationContext, "Agregado con exito ", Toast.LENGTH_LONG).show()
                                    }else {
                                        Toast.makeText(applicationContext, "Algo fallo ", Toast.LENGTH_LONG).show()
                                    }
                                }
                            } else{
                                Alert()
                            }
                        }
                    }else {
                        Toast.makeText(this, "Numero incorrecto", Toast.LENGTH_LONG).show()
                    }
                }else{

                    Toast.makeText(this, "El password debe contener mas de 6 caracteres", Toast.LENGTH_LONG).show()
                }
            }
        }
    }
    private fun isValidEmail(email: String): Boolean {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun Alert(){

        val bulder = AlertDialog.Builder(this)
        bulder.setTitle("error")
        bulder.setMessage("Se ha produccido un error autenticando al usuario")
        bulder.setPositiveButton("Aceptar", null)
        val dialog: AlertDialog = bulder.create()
        dialog.show()
    }

    fun ShowHome(context: Context){
        var log = Loading(this)
        log.startL()
        var handler = Handler()
        handler.postDelayed(object :Runnable{
            override fun run() {
                val intent = Intent(context, LoginActivity::class.java)
                startActivity(intent)
                finish()
                log.isDismiss()
            }

        }, 2500)

    }


    fun getImage() {
        if (CheckPermission()) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestLocationPermission()
                return
            }
                val intent = Intent(Intent.ACTION_PICK)
                intent.type = "image/*"
                startActivityForResult(intent, PICKIMAGE)
        }else{
            requestLocationPermission()
        }
    }



    private fun CheckPermission(): Boolean{
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            return true
        }
        return false

    }

    private fun requestLocationPermission(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)){
            Toast.makeText(this, "Ve a ajustes y acepta los permisos", Toast.LENGTH_LONG).show()

        }else {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                PICKIMAGE
            )

        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == PICKIMAGE) {
            imageUri = data?.data
            binding.idFotoUser.setImageURI(imageUri)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == PICKIMAGE){
            if (grantResults.isNotEmpty()&& grantResults[0]==PackageManager.PERMISSION_GRANTED){
                getImage()
            }else {

                Toast.makeText(this, "Fallo", Toast.LENGTH_LONG).show()

            }
        }
    }


}