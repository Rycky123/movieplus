package com.example.movieplus.view

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import androidx.lifecycle.observe
import com.example.movieplus.R
import com.example.movieplus.model.CheckConnection

var connetion = false
class SplastActivity : AppCompatActivity() {
    private lateinit var checkNetworkConnetion: CheckConnection
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splast)
        isNetworkAvailable(this)
        callNetConnection()

        val background = object : Thread() {
            override fun run() {
                try {
                    Thread.sleep(2500)
                    val i = Intent()
                    val prefManager2 =
                        PreferenceManager.getDefaultSharedPreferences(this@SplastActivity)
                    if (!prefManager2.getBoolean("cache", false)) {

                        val prefEditor = prefManager2.edit()
                        prefEditor.putBoolean("cache", true)
                        prefEditor.apply()
                        i.setClass(baseContext, LoginActivity::class.java)

                        startActivity(i)
                    }else {
                        i.setClass(baseContext, LoginActivity::class.java)
                        startActivity(i)
                    }
                    overridePendingTransition(
                        R.anim.slide_in_right,
                        R.anim.slide_out_left)
                    finish()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }

        background.start()
    }

    private fun callNetConnection() {
        checkNetworkConnetion = CheckConnection(application)
        checkNetworkConnetion.observe(this) { isConnected ->
            connetion = isConnected
        }
    }

    fun isNetworkAvailable(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo: NetworkInfo?
        activeNetworkInfo = cm.activeNetworkInfo
        connetion = activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting
    }
}