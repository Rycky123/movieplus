package com.example.movieplus.view

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.movieplus.R
import com.example.movieplus.databinding.ActivityLoginBinding
import com.example.movieplus.model.Loading
import com.example.movieplus.model.data.database.DatabaseSQL
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {

    lateinit var binding: ActivityLoginBinding
    private val db = FirebaseFirestore.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.btnSign.setOnClickListener {
           verification()
        }

        binding.btnRegistrarse.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)


        }
    }


    fun verification(){
        if (binding.loginUser.text.toString().isEmpty() || binding.loginPass.text.toString().isEmpty()) {
            if (binding.loginUser.text.toString().isEmpty() && binding.loginPass.text.toString().isEmpty()) {
                binding.loginUser.setError("Campo Requerido")
                binding.loginPass.setError("Campo Requerido")
            } else {
                if (binding.loginPass.text.toString().isEmpty())
                    binding.loginPass.setError("Campo Requerido")

                if (binding.loginPass.text.toString().isEmpty()) {
                    binding.loginPass.setError("Campo Requerido")
                }
            }
        }else {

            if (connetion == true){

                FirebaseAuth.getInstance().signInWithEmailAndPassword(binding.loginUser.text.toString(), binding.loginPass.text.toString())
                    .addOnCompleteListener {

                        if (it.isSuccessful) {
                            ShowHome(this, binding.loginUser.text.toString())
                        } else {
                            Alert()
                        }
                    }
            } else {
                val databaseHandler: DatabaseSQL = DatabaseSQL(this)
                val estatus = databaseHandler.getUserIdByEmail(binding.loginUser.text.toString(), binding.loginPass.text.toString())
                if(estatus > 0 ){
                    Toast.makeText(applicationContext, "Usuario Identificado ", Toast.LENGTH_LONG).show()
                    val intent = Intent(this, MainActivity::class.java)
                    intent.putExtra("id", binding.loginUser.text.toString())
                    startActivity(intent)
                    finish()
                }else {
                    Toast.makeText(applicationContext, "Verifique sus datos ", Toast.LENGTH_LONG).show()
                }

            }

        }
    }
    fun Alert(){

        val bulder = AlertDialog.Builder(this)
        bulder.setTitle("error")
        bulder.setMessage("Se ha produccido un error autenticando al usuario")
        bulder.setPositiveButton("Aceptar", null)
        val dialog: AlertDialog = bulder.create()
        if (! isFinishing()) {

            dialog.show();

        }
        //   dialog.show()
    }

    fun ShowHome(context: Context, email:String){
        var log = Loading(this)
        log.startL()
        var handler = Handler()
        handler.postDelayed(object :Runnable{
            override fun run() {
                val intent = Intent(context, MainActivity::class.java)
                intent.putExtra("id", email)
                startActivity(intent)
                finish()
                log.isDismiss()
            }

        }, 2000)

    }

}