package com.example.movieplus.view

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.media.audiofx.BassBoost
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.lifecycle.observe
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.example.movieplus.R
import com.example.movieplus.model.CheckConnection
import com.example.movieplus.view.MapsFragment.Companion.REQUEST_CODE
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import kotlin.properties.Delegates
var email = ""
@AndroidEntryPoint
class MainActivity @Inject constructor(): AppCompatActivity() {

    private lateinit var fusedLocationProvider: FusedLocationProviderClient
    private lateinit var checkNetworkConnetion: CheckConnection

    private val db = FirebaseFirestore.getInstance()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fusedLocationProvider = LocationServices.getFusedLocationProviderClient(this)
        getLocation()
        callNetConnection()
            email = intent.getStringExtra("id").toString()
        Log.e("el email", email)
       val btnNavigation = findViewById<BottomNavigationView>(R.id.btn_nav)
        val navController = Navigation.findNavController(this,
            R.id.host_fragment
        )

        NavigationUI.setupWithNavController(btnNavigation, navController)


    }

   private fun getLocation(){
        if (CheckPermission()){
            if (isLocationEnable()){

                if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                   requestLocationPermission()
                    return
                }
                fusedLocationProvider.lastLocation.addOnCompleteListener(this){
                    task->
                    val location: Location? = task.result
                    if (location == null){

                        Toast.makeText(this, "null", Toast.LENGTH_LONG).show()
                    }else {

                        Toast.makeText(this, "success", Toast.LENGTH_LONG).show()

                        var handler = Handler()
                        handler.postDelayed(object : Runnable {
                            override fun run() {
                                if (connetion==true){
                                    db.collection("Usuarios").document(email).update("Lat",location.latitude.toString())
                                    db.collection("Usuarios").document(email).update("Log",location.longitude.toString())
                                }

                                Log.e("LATITUD", location.latitude.toString())
                                Log.e("Longuitud",location.longitude.toString())
                                handler.postDelayed(this, 900000)
                            }
                        }, 1)




                    }
                }

            }else{

                Toast.makeText(this, "Loacation on", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)

            }
        }else{
            requestLocationPermission()
        }
   }

    private fun isLocationEnable():Boolean{

        var locationManager:LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)|| locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)

    }

    private fun CheckPermission(): Boolean{
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            return true
        }
        return false

        }


    private fun callNetConnection() {
        checkNetworkConnetion = CheckConnection(application)
        checkNetworkConnetion.observe(this) { isConnected ->
            if(isConnected){
                connetion=true
            }else{false}


        }
    }


    private fun requestLocationPermission(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_FINE_LOCATION) ||
            ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)){
            Toast.makeText(this, "Ve a ajustes y acepta los permisos", Toast.LENGTH_LONG).show()

        }else {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
              REQUEST_CODE
            )

        }
    }



    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_CODE){
            if (grantResults.isNotEmpty()&& grantResults[0]==PackageManager.PERMISSION_GRANTED){
                Log.e("permiso", "hecho")
                        getLocation()
            }else {

                Toast.makeText(this, "Fallo", Toast.LENGTH_LONG).show()

            }
        }
    }


    }

