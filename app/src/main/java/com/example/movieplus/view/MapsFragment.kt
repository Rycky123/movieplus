package com.example.movieplus.view

import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.fragment.app.Fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.observe
import com.example.movieplus.R
import com.example.movieplus.model.CheckConnection
import com.google.android.gms.location.LocationServices

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.firestore.FirebaseFirestore
import dagger.hilt.android.AndroidEntryPoint
import java.util.jar.Manifest
@AndroidEntryPoint
class MapsFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMyLocationClickListener {
    private lateinit var checkNetworkConnetion: CheckConnection
    private val db = FirebaseFirestore.getInstance()

    private lateinit var map:GoogleMap

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        CreateMap()
        isNetworkAvailable(requireContext())
        callNetConnection()

    }


    private fun CreateMap(){
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment?.getMapAsync(this)
    }


    private fun CreateMarker(){

        if (connetion == true){
            var lat = ""
            var log= ""
            db.collection("Usuarios").document(email).get().addOnSuccessListener {
                lat = (it.get("Lat")as String?).toString()
                log = (it.get("Log")as String?).toString()
                Log.e("Esto traje", lat)
                val cod = LatLng(lat.toDouble(), log.toDouble())
                val marker = MarkerOptions().position(cod).title("Hola mundo")
                map.addMarker(marker)
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(cod, 12f), 4000, null)
            }
        }


    }

    private fun CheckPermission() = ContextCompat.checkSelfPermission(requireContext(),
        android.Manifest.permission.ACCESS_FINE_LOCATION) ==  PackageManager.PERMISSION_GRANTED

        private fun requestLocation(){
            if (!::map.isInitialized)return
        if(CheckPermission()){
            map.isMyLocationEnabled = true
        }else {
            requestLocationPermission()
        }

        }

    private fun requestLocationPermission(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)){
            Toast.makeText(requireContext(), "Ve a ajustes y acepta los permisos", Toast.LENGTH_LONG).show()

        }else {
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                Companion.REQUEST_CODE
            )

        }
    }


    private fun callNetConnection() {
        checkNetworkConnetion = CheckConnection(requireActivity().application)
        checkNetworkConnetion.observe(requireActivity()) { isConnected ->
            if(isConnected){
                connetion=true
            }else{false}
        }
    }

    fun isNetworkAvailable(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        var activeNetworkInfo: NetworkInfo? = null
        activeNetworkInfo = cm.activeNetworkInfo
        connetion = activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when(requestCode){
            Companion.REQUEST_CODE -> if(grantResults.isNotEmpty() && grantResults[0]==PackageManager.PERMISSION_GRANTED){
            map.isMyLocationEnabled= true
        }else {
            Toast.makeText(requireContext(), "Para activar la localizacion ve a los ajustes", Toast.LENGTH_LONG).show()
        }
            else -> {}
        }
    }

    override fun onMapReady(p0: GoogleMap) {
        map = p0
        CreateMarker()
        map.setOnMyLocationClickListener(this)
        requestLocation()
    }

    override fun onMyLocationClick(p0: Location) {
        Toast.makeText(requireContext(), "Estas en: ${p0.latitude}, ${p0.longitude}", Toast.LENGTH_LONG).show()
    }

    companion object {
        const val REQUEST_CODE = 0
    }


}