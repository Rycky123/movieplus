package com.example.movieplus.view

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.recyclerview.widget.GridLayoutManager
import com.example.movieplus.databinding.FragmentHomeBinding
import com.example.movieplus.model.Adapters.PopularAdapter
import com.example.movieplus.model.CheckConnection
import com.example.movieplus.model.domain.ModelMovie
import com.example.movieplus.viewModel.viewModelPopularMovie
import dagger.hilt.android.AndroidEntryPoint
import kotlin.collections.ArrayList

@AndroidEntryPoint
class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding
    private lateinit var checkNetworkConnetion: CheckConnection
    var popularMovie = PopularAdapter()
     val mvvm: viewModelPopularMovie by viewModels()
   // lateinit var mv: viewModelPopularMovie

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            binding = FragmentHomeBinding.inflate(inflater, container, false)
      //  mv = ViewModelProviders.of(this)[viewModelPopularMovie::class.java]


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isNetworkAvailable(requireContext())
        callNetConnection()
        mvvm.CreateMovie()
        ObservePopularMovie()
        RecyclerPopular()
    }

    fun RecyclerPopular(){
        binding.rvPopularMovies.apply {
            layoutManager = GridLayoutManager(requireContext(), 3)
            adapter = popularMovie
        }
    }

    fun ObservePopularMovie(){
        mvvm.ObseverPopularLiveData().observe(viewLifecycleOwner, Observer { pop ->
        popularMovie.setMovie(pop as ArrayList<ModelMovie>)
        })

    }

    private fun callNetConnection() {
        checkNetworkConnetion = CheckConnection(requireActivity().application)
        checkNetworkConnetion.observe(requireActivity()) { isConnected ->
            connetion = isConnected
        }
    }

    fun isNetworkAvailable(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        var activeNetworkInfo: NetworkInfo? = null
        activeNetworkInfo = cm.activeNetworkInfo
        connetion = activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting
    }

}


