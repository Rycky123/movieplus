package com.example.movieplus.view

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.observe
import com.example.movieplus.R
import com.example.movieplus.model.CheckConnection
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PerfilFragment : Fragment() {

    private lateinit var checkNetworkConnetion: CheckConnection
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_perfil, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isNetworkAvailable(requireContext())
        callNetConnection()
    }

    private fun callNetConnection() {
        checkNetworkConnetion = CheckConnection(requireActivity().application)
        checkNetworkConnetion.observe(requireActivity()) { isConnected ->
            if(isConnected){
                connetion=true
            }else{false}
        }
    }

    fun isNetworkAvailable(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        var activeNetworkInfo: NetworkInfo? = null
        activeNetworkInfo = cm.activeNetworkInfo
        connetion = activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting
    }

}