package com.example.movieplus.model.data.database.Entity

import android.graphics.Movie
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.movieplus.model.domain.ModelMovie

@Entity(tableName = "movie_table")
data class MovieEntity(
    @PrimaryKey(autoGenerate = true)
   val id: Int = 0,
    val backdrop_path: String,
   val title: String)

fun ModelMovie.toDatabase() = MovieEntity( backdrop_path ="https://image.tmdb.org/t/p/w500"+backdrop_path, title = title)