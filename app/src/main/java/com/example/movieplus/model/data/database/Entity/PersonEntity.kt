package com.example.movieplus.model.data.database.Entity


class PersonEntity (

    val id: Int = 0,
    val Name: String,
    val Password:String,
    val Tel: String,
    val Email:String,
    val Direccion:String,
    val Foto: String)
