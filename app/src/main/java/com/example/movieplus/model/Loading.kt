package com.example.movieplus.model

import android.app.Activity
import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import com.example.movieplus.R

class Loading (val mActiv: Activity){


    private lateinit var isdialog:AlertDialog

    fun startL(){
        var inflate = mActiv.layoutInflater
        val dialogView = inflate.inflate(R.layout.pantalla_carga, null)

        val bul = AlertDialog.Builder(mActiv).setView(dialogView)
         isdialog = bul.show()
        isdialog.setCancelable(false)
        isdialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

    }

    fun isDismiss(){
        isdialog.dismiss()

    }
}