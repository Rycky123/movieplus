package com.example.movieplus.model.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.example.movieplus.model.data.Convertter
import com.example.movieplus.model.data.database.Dao.Dao
import com.example.movieplus.model.data.database.Entity.MovieEntity
import com.example.movieplus.model.data.database.Entity.PersonEntity

@Database(entities = arrayOf(MovieEntity::class), version = 1, exportSchema = false)
@TypeConverters(Convertter::class)
abstract class MovieDataBase: RoomDatabase()  {

    abstract fun getMovieDaoDatabase():Dao
}