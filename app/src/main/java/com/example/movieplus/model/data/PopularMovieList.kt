package com.example.movieplus.model.data

data class PopularMovieList(
    val page: Int,
    val results: List<PopularMovie>,
    val total_pages: Int,
    val total_results: Int
)