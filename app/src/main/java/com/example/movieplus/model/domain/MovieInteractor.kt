package com.example.movieplus.model.domain

import android.util.Log
import com.example.movieplus.model.MovieRepository
import com.example.movieplus.model.data.PopularMovie
import com.example.movieplus.model.data.database.Entity.PersonEntity
import com.example.movieplus.model.data.database.Entity.toDatabase
import com.example.movieplus.view.connetion
import javax.inject.Inject

class MovieInteractor @Inject constructor(
     val repository: MovieRepository
) {

    suspend operator fun invoke(): List<ModelMovie>{



        return if (connetion == true){
            val movie = repository.getAllMovies2()
            repository.clearDatabase()
            repository.inserteMovie(movie.map { it.toDatabase() })
             movie
        }else {
            repository.getAllMoviesDatabase()
        }
    }

}