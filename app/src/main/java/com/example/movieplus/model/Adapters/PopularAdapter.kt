package com.example.movieplus.model.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.movieplus.R
import com.example.movieplus.model.data.PopularMovie
import com.example.movieplus.model.domain.ModelMovie

class PopularAdapter(): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var moviesList = ArrayList<ModelMovie>()

    fun setMovie(movieList1: ArrayList<ModelMovie>){
        this.moviesList = movieList1
        notifyDataSetChanged()
    }

    class CheckMovie(view:View): RecyclerView.ViewHolder(view){
    var movie = view.findViewById<ImageView>(R.id.img_movie)
     var name = view.findViewById<TextView>(R.id.tv_movie_name)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view:View = inflater.inflate(R.layout.popular_item, parent, false)

        return CheckMovie(view)
    }
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        var view: CheckMovie = holder as CheckMovie

        Glide.with(holder.itemView).load("https://image.tmdb.org/t/p/w500"+moviesList[position].backdrop_path).into(view.movie)
        view.name.text = moviesList[position].title
    }

    override fun getItemCount(): Int {
        return moviesList.size
    }


}