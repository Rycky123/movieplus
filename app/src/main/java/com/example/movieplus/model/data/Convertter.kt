package com.example.movieplus.model.data

import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


@TypeConverters
class Convertter {

@TypeConverter
fun fromListToJson(value: List<Int>):String{
    val json =Gson()
    val tipe = object : TypeToken<List<Int>>(){}.type
    return json.toJson(value, tipe)
}

    @TypeConverter
    fun toGroupTaskMemberList(value: String): List<Int> {
        val gson = Gson()
        val type = object : TypeToken<List<Int>>() {}.type
        return gson.fromJson(value, type)
    }

}