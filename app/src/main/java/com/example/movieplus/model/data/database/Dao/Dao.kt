package com.example.movieplus.model.data.database.Dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.movieplus.model.data.PopularMovie
import com.example.movieplus.model.data.database.Entity.MovieEntity
import com.example.movieplus.model.data.database.Entity.PersonEntity

@Dao
interface Dao {
    @Query("SELECT * FROM movie_table")
    suspend fun getMoviesDao():List<MovieEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovie(movie: List<MovieEntity>)


    @Query("DELETE FROM movie_table")
    suspend fun deleteAll()

}