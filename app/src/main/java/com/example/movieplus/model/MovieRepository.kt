package com.example.movieplus.model

import android.util.Log
import com.example.movieplus.model.data.PopularMovie
import com.example.movieplus.model.data.database.Dao.Dao
import com.example.movieplus.model.data.database.Entity.MovieEntity
import com.example.movieplus.model.data.database.Entity.PersonEntity
import com.example.movieplus.model.domain.ModelMovie
import com.example.movieplus.model.domain.toDomain
import com.example.movieplus.model.network.MovieService
import javax.inject.Inject

class MovieRepository @Inject constructor(

    val movieDao: Dao,
    val api: MovieService
){


    suspend fun getAllMovies2():List<ModelMovie>{
        val response: List<PopularMovie> = api.getmovies()
        return response.map { it.toDomain() }

    }

    suspend fun getAllMoviesDatabase():List<ModelMovie>{
        val response: List<MovieEntity> = movieDao.getMoviesDao()
        return response.map { it.toDomain() }

    }

    suspend fun inserteMovie(movie : List<MovieEntity>){
        movieDao.insertMovie(movie)
    }

    suspend  fun clearDatabase() {
        movieDao.deleteAll()
    }
}