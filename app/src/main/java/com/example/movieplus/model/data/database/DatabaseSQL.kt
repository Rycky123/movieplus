package com.example.movieplus.model.data.database

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.example.movieplus.model.data.database.Entity.PersonEntity

class DatabaseSQL(context: Context):
    SQLiteOpenHelper(context,DATABASE_NAME, null, DATABASE_VERSION) {


    companion object{
        private const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "UserDatabase"
        private const val TABLE_REGISTERS = "RegisterTable"

        private const val KEY_ID = "_id"
        private const val KEY_NAME = "nombre"
        private const val KEY_PAS = "password"
        private const val KEY_TEL = "telefono"
        private const val KEY_EMAIL = "Email"
        private const val KEY_Direccion = "Direccion"
        private const val KEY_FOTO = "FOTO"

    }

    override fun onCreate(p0: SQLiteDatabase?) {
        val CREATE_REGISTER_TABLE = ("CREATE TABLE " + TABLE_REGISTERS + "("+ KEY_ID + " INTEGER PRIMARY KEY, " +  KEY_NAME + " TEXT, "+
                KEY_TEL+ " TEXT, "+ KEY_EMAIL + " TEXT, " + KEY_PAS + " TEXT, " + KEY_Direccion + " TEXT, "+ KEY_FOTO + " TEXT " + ")")
        p0?.execSQL(CREATE_REGISTER_TABLE)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        p0!!.execSQL("DROP TABLE IF EXISTS "+ TABLE_REGISTERS)
        onCreate(p0)
    }


    fun addUser(user: PersonEntity): Long {

        val db = this.writableDatabase
        val contentValues = ContentValues()

        contentValues.put(KEY_NAME, user.Name)
        contentValues.put(KEY_TEL, user.Tel)
        contentValues.put(KEY_PAS, user.Password)
        contentValues.put(KEY_EMAIL, user.Email)
        contentValues.put(KEY_Direccion, user.Direccion)
        contentValues.put(KEY_FOTO, user.Foto)

        val sucess = db.insert(TABLE_REGISTERS, null, contentValues)
        db.close()
        return sucess
    }

    @SuppressLint("Range")
    fun getUserIdByEmail(email: String , pwd: String): Int {
        val db = this.readableDatabase
        val cursor = db.query(TABLE_REGISTERS,
            null,
            "$KEY_EMAIL = ? AND $KEY_PAS = ?", arrayOf(email, pwd),
            null,
            null,
            null
        )
        if (cursor.getCount() < 1) {
            return 0;
        } else {
            cursor.moveToFirst()
            val userID: Int = cursor.getString(cursor.getColumnIndex(KEY_ID)).toInt()
            return userID;

        }
    }

}