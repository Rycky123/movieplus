package com.example.movieplus.model.network

import android.content.Context
import androidx.room.Room
import com.example.movieplus.model.data.database.MovieDataBase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {

    private const val MOVIE_DATABASE_NAME = "movie_database"

    @Singleton
    @Provides
    fun ProviderRoom(@ApplicationContext context: Context)= Room.databaseBuilder(context, MovieDataBase::class.java, MOVIE_DATABASE_NAME).build()

    @Singleton
    @Provides
    fun ProviderDaoMovie(db: MovieDataBase) = db.getMovieDaoDatabase()
}