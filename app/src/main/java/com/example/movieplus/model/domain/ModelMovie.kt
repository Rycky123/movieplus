package com.example.movieplus.model.domain

import com.example.movieplus.model.data.PopularMovie
import com.example.movieplus.model.data.database.Entity.MovieEntity

data class ModelMovie(val backdrop_path: String,val title: String )

fun PopularMovie.toDomain() = ModelMovie(backdrop_path, title)
fun MovieEntity.toDomain() = ModelMovie(backdrop_path, title)