package com.example.movieplus.model.network

import android.util.Log
import com.example.movieplus.model.ApiService
import com.example.movieplus.model.data.PopularMovie
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MovieService @Inject constructor(private val retro: ApiService) {


    suspend fun getmovies(): List<PopularMovie> {


        return withContext(Dispatchers.IO){
            val response = retro.getPopularMovieApi2()
            response.body()?.results ?: emptyList()
        }



    }

}