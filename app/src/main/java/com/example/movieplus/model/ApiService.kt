package com.example.movieplus.model

import com.example.movieplus.model.data.PopularMovieList
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {
    @GET("3/movie/popular?api_key=68f9975d31155b58cb403f64c65c9fb2")
   suspend fun getPopularMovieApi2():Response<PopularMovieList>

}