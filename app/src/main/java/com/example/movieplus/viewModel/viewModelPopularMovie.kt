package com.example.movieplus.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.movieplus.model.domain.ModelMovie
import com.example.movieplus.model.domain.MovieInteractor
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class viewModelPopularMovie @Inject constructor(
     var interactor: MovieInteractor
): ViewModel() {

    private var MovieLiveData = MutableLiveData<List<ModelMovie>>()


    fun ObseverPopularLiveData():LiveData<List<ModelMovie>>{
        return MovieLiveData

    }

    fun CreateMovie(){
        viewModelScope.launch {

            Log.e("Estoy aqui","ViewModelPopular")
            val result = interactor()
            if(!result.isNullOrEmpty()){

                MovieLiveData.postValue(result)
            }
        }


    }


}